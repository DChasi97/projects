/*H**********************************************************************
 *
 *	This is a skeleton to guide development of Othello engines to be used
 *	with the Cloud Tournament Engine (CTE). CTE runs tournaments between
 *	game engines and displays the results on a web page which is available on
 *	campus (!!!INSERT URL!!!). Any output produced by your implementations will
 *	be available for download on the web page.
 *
 *	The socket communication required for DTE is handled in the main method,
 *	which is provided with the skeleton. All socket communication is performed
 *	at rank 0.
 *
 *	Board co-ordinates for moves start at the top left corner of the board i.e.
 *	if your engine wishes to place a piece at the top left corner, the "gen_move"
 *	function must return "00".
 *
 *	The match is played by making alternating calls to each engine's "gen_move"
 *	and "play_move" functions. The progression of a match is as follows:
 *		1. Call gen_move for black player
 *		2. Call play_move for white player, providing the black player's move
 *		3. Call gen move for white player
 *		4. Call play_move for black player, providing the white player's move
 *		.
 *		.
 *		.
 *		N. A player makes the final move and "game_over" is called for both players
 *
 *	IMPORTANT NOTE:
 *		Any output that you would like to see (for debugging purposes) needs
 *		to be written to file. This can be done using file FP, and fprintf(),
 *		don't forget to flush the stream.
 *		I would suggest writing a method to make this
 *		easier, I'll leave that to you.
 *		The file name is passed as argv[4], feel free to change to whatever suits you.
 *H***********************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<mpi.h>
#include<time.h>

const int EMPTY = 0;
const int BLACK = 1;
const int WHITE = 2;
const int OUTER = 3;
const int ALLDIRECTIONS[8]={-11, -10, -9, -1, 1, 9, 10, 11};
const int BOARDSIZE=100;
const int MAX_DEPTH = 5;
const int WEIGHTS_LOC[100] = {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
                              0,120,-20, 20,  5,  5, 20,-20,120,  0,
                              0,-20,-40, -5, -5, -5, -5,-40,-20,  0,
                              0, 20, -5, 15,  3,  3, 15, -5, 20,  0,
                              0,  5, -5,  3,  3,  3,  3, -5,  5,  0,
                              0,  5, -5,  3,  3,  3,  3, -5,  5,  0,
                              0, 20, -5, 15,  3,  3, 15, -5, 20,  0,
                              0,-20,-40, -5, -5, -5, -5,-40,-20,  0,
                              0,120,-20, 20,  5,  5, 20,-20,120,  0,
                              0, 0,  0,  0,  0,  0,  0,  0,  0,   0};



char* gen_move();
void play_move(char *move);
void game_over();
void run_worker();
void initialise();

int* initialboard(void);
int *legalmoves (int player, int *localboard);
int legalp (int move, int player, int *board);
int validp (int move);
int wouldflip (int move, int dir, int player, int *board);
int opponent (int player);
int findbracketingpiece(int square, int dir, int player, int *board);
int strategy();
void makemove (int move, int player, int *board);
void makeflips (int move, int dir, int player, int *board);
int get_loc(char* movestring);
char* get_move_string(int loc);
void printboard();
char nameof(int piece);
int count (int player, int * board);
void send_board();
int count_legalmoves(int player, int *localboard);
int weighted_vals(int player, int *board);
int mini_max_alpha(int *board, int alpha, int beta, int depth, int player);

void printlocalboard(int *localboard);




int my_colour;
int time_limit;
int running;
int rank;
int size;
int *board;
int *positions;
int *final_scorediffs;
int firstrun = 1;
FILE *fp;
int msg_avail;
int more_work = 500;
int done = 200;
int end = 300;
MPI_Status status;
int b_size;

struct {
  int move;
  int score_diff;
} loc_data, global_data;



int main(int argc , char *argv[]) {
    int socket_desc, port, msg_len;
    char *ip, *cmd, *opponent_move, *my_move;
    char msg_buf[15], len_buf[2];
    struct sockaddr_in server;
    ip = argv[1];
    port = atoi(argv[2]);
    time_limit = atoi(argv[3]);
    my_colour = EMPTY;
    running = 1;

    /* starts MPI */
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);	/* get current process id */
    MPI_Comm_size(MPI_COMM_WORLD, &size);	/* get number of processes */
    // Rank 0 is responsible for handling communication with the server

    if (rank == 0){

        fp = fopen(argv[4], "w");
        fprintf(fp, "This is an example of output written to file.\n");
        fflush(fp);
        initialise();
        socket_desc = socket(AF_INET , SOCK_STREAM , 0);
        if (socket_desc == -1) {
            fprintf(fp, "Could not create socket\n");
            fflush(fp);
            return -1;
        }
        server.sin_addr.s_addr = inet_addr(ip);
        server.sin_family = AF_INET;
        server.sin_port = htons(port);

        //Connect to remote server
        if (connect(socket_desc , (struct sockaddr *)&server , sizeof(server)) < 0){

            fprintf(fp, "Connect error\n");
            fflush(fp);
            return -1;
        }
        fprintf(fp, "Connected\n");
        fflush(fp);
        if (socket_desc == -1){
            return 1;
        }
        while (running == 1){
            if (firstrun ==1) {
                char tempColour[1];
                if(recv(socket_desc, tempColour , 1, 0) < 0){
                    fprintf(fp,"Receive failed\n");
                    fflush(fp);
                    running = 0;
                    break;
                }
                my_colour = atoi(tempColour);
                fprintf(fp,"Player colour is: %d\n", my_colour);
                fflush(fp);
                firstrun = 2;
            }


            if(recv(socket_desc, len_buf , 2, 0) < 0){


                fprintf(fp,"Receive failed\n");
                fflush(fp);
                running = 0;
                break;
            }

            msg_len = atoi(len_buf);


            if(recv(socket_desc, msg_buf, msg_len, 0) < 0){
                fprintf(fp,"Receive failed\n");
                fflush(fp);
                running = 0;
                break;
            }


            msg_buf[msg_len] = '\0';
            cmd = strtok(msg_buf, " ");

            if (strcmp(cmd, "game_over") == 0){
                running = 0;
                MPI_Bcast(&running, 1, MPI_INT, 0 , MPI_COMM_WORLD);
                fprintf(fp, "Game over\n");
                fflush(fp);
                free(board);
                break;

            } else if (strcmp(cmd, "gen_move") == 0){
                MPI_Bcast(&running, 1, MPI_INT, 0 , MPI_COMM_WORLD);
                MPI_Bcast(&my_colour, 1, MPI_INT, 0 , MPI_COMM_WORLD);
                my_move = gen_move();
                if (send(socket_desc, my_move, strlen(my_move) , 0) < 0){
                    running = 0;
                    fprintf(fp,"Move send failed\n");
                    fflush(fp);
                    break;
                }
                printboard();
            } else if (strcmp(cmd, "play_move") == 0){
                opponent_move = strtok(NULL, " ");
                play_move(opponent_move);
                //MPI_Bcast(board, BOARDSIZE, MPI_INT, 0 , MPI_COMM_WORLD);
                printboard();

            }
            memset(len_buf, 0, 2);
            memset(msg_buf, 0, 15);
        }
        game_over();

    } else {

        run_worker(rank);
        MPI_Finalize();
    }
    return 0;
}

/*
	Called at the start of execution on all ranks
 */
void initialise(){
    int i;
    running = 1;
    board = (int *)malloc(BOARDSIZE * sizeof(int));
    for (i = 0; i<=9; i++) board[i]=OUTER;
    for (i = 10; i<=89; i++) {
        if (i%10 >= 1 && i%10 <= 8) board[i]=EMPTY; else board[i]=OUTER;
    }
    for (i = 90; i<=99; i++) board[i]=OUTER;
    board[44]=WHITE; board[45]=BLACK; board[54]=BLACK; board[55]=WHITE;

}



/*
	Called at the start of execution on all ranks except for rank 0.
	This is where messages are passed between workers to guide the search.
 */
void run_worker(int rank){

    int *tempboard, *moves, terminated ;


    int alpha = -90;
    int beta = 90;


    initialise();

    MPI_Bcast(&running, 1, MPI_INT, 0 , MPI_COMM_WORLD);
    MPI_Bcast(&my_colour, 1, MPI_INT, 0 , MPI_COMM_WORLD);




    while(running == 1) {

        if (running != 1) break;

        MPI_Bcast(board, BOARDSIZE, MPI_INT, 0 , MPI_COMM_WORLD);
        moves = legalmoves(my_colour, board);

        if(moves[0] > 0) {
          terminated = 1;
//checks if terminate flag has been sent recieves work
        while(terminated) {

          MPI_Iprobe(0, more_work, MPI_COMM_WORLD, &msg_avail , &status);
          while(msg_avail){
            MPI_Recv(&loc_data, 1 , MPI_2INT, 0,  more_work, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

            tempboard =(int *)malloc(BOARDSIZE * sizeof(int));
            memcpy(tempboard, board, BOARDSIZE * sizeof(int));
            makemove(loc_data.move, my_colour, tempboard);
            loc_data.score_diff = mini_max_alpha(tempboard , alpha , beta , MAX_DEPTH, my_colour);

            MPI_Send(&loc_data, 1 , MPI_2INT, 0, done, MPI_COMM_WORLD);
            MPI_Iprobe(0, more_work, MPI_COMM_WORLD, &msg_avail , &status);
            free(tempboard);
          }
//waiting to recieve terminate message
          MPI_Iprobe(0, 5000, MPI_COMM_WORLD, &msg_avail , &status);
          while(msg_avail){

              MPI_Recv(&msg_avail, 1 , MPI_INT, 0,  5000, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
              terminated = 0;
              free(moves);
              MPI_Iprobe(0, 5000, MPI_COMM_WORLD, &msg_avail , &status);
          }

        }

      }

    }
  }

/*minimax algorithm maximises the player and minimizes the opponent this algorithm
assumes that the opponent player is also playing at its maximum*/
int mini_max_alpha(int *localboard, int alpha, int beta, int depth, int player){
  int width;
  int cutoff;
  int i ,val, *moves, score_diff,  *tempboard;

  moves = legalmoves(player,localboard);
  tempboard =(int *)malloc(BOARDSIZE * sizeof(int));
  width = moves[0];
  score_diff = 0;

  if(depth <= 0){
    /*return evaluate the position*/
    score_diff = weighted_vals(player, localboard);
    return score_diff;
    free(moves);

  }
  /*generate moves*/
  if(width == 0){
    /*return evaluate position*/
    score_diff = weighted_vals(player, localboard);
    return score_diff;
    free(moves);

  }

  cutoff = 0;
  i = 1;

  while( (i<= width) && (cutoff == 0) ){

        memcpy(tempboard, localboard, BOARDSIZE * sizeof(int));
        makemove(moves[i], player, tempboard);
        val = mini_max_alpha(tempboard, alpha , beta , depth-1, opponent(player));
// max player is the max node
        if((player == my_colour) && (val > alpha)){
          alpha = val;
//min player is the min node
        } else if((player != my_colour) && (val < beta)){
          beta = val;

        }
        if(alpha >= beta){
          cutoff = 1;
        }
        i++;
    }

   if((player == my_colour)){
     return alpha;
     free(moves);

   } else {
     return beta;
     free(moves);
   }
   free(tempboard);
}
// calculating the score given by each position
int weighted_vals(int player, int *board){

  int opponent_val;
  int player_val;

  int coin_parity;
  int mobility;
  int corners;


  int max_player_coins;
  int min_player_coins;
  int max_player_moves;
  int min_player_moves;
  int max_player_corners;
  int min_player_corners;
  int weighted_val;


  opponent_val = 0;
  player_val = 0;
  max_player_coins = 0;
  min_player_coins = 0;
  max_player_moves = 0;
  min_player_moves = 0;
  max_player_corners = 0;
  min_player_corners= 0;
  weighted_val = 0;

  max_player_coins = count(player, board);
  min_player_coins = count(opponent(player), board);

  max_player_moves = count_legalmoves(player, board);
  min_player_moves = count_legalmoves(opponent(player), board);

  for ( int i=1; i<=88; i++){
      if ((board[11] == player) || (board[18] == player) || (board[88] == player) || (board[81]== player))
       max_player_corners++;
      if ((board[11] == opponent(player)) || (board[18] == opponent(player)) || (board[88] == opponent(player)) || (board[81]== opponent(player)))
      min_player_corners++;
      if (board[i] == player) player_val = player_val + WEIGHTS_LOC[i];
      if(board[i] == opponent(player)) opponent_val = opponent_val + WEIGHTS_LOC[i];

    }

    coin_parity = 100 * ((max_player_coins - min_player_coins)/(max_player_coins + min_player_coins));

    if((max_player_moves+ min_player_moves) != 0 ){
      mobility = 100 * ((max_player_moves - min_player_moves)/(max_player_moves + min_player_moves));

    } else {
      mobility = 0;
    }

    if((max_player_corners + min_player_corners) != 0){
      corners = 100 * ((max_player_corners - min_player_corners)/(max_player_corners + min_player_corners));
    } else {
      corners = 0;
    }

    weighted_val = corners + mobility + coin_parity + (player_val - opponent_val);
    return  weighted_val;
}



/*
	Called when your engine is required to make a move. It must return
	a string of the form "xy", where x and y represent the row and
	column where your piece is placed, respectively.

	play_move will not be called for your own engine's moves, so you
	must apply the move generated here to any relevant data structures
	before returning.
 */
char* gen_move(){
    //int loc;
    //fprintf(stderr, "in gen move-------\n");
    char *move;
    int *tempboard;
    int *moves;
    int maximum;
    int temp_move;
    int i, j, init_send;
    int recv;
    int alpha = -90;
    int beta = 90;
    int terminated;



    maximum = -5000;
    temp_move = 0;
    i = 1;
    recv = 0;

  if (my_colour == EMPTY){
      my_colour = BLACK;
  }
// broadcasting current board
  MPI_Bcast(board, BOARDSIZE, MPI_INT, 0 , MPI_COMM_WORLD);

  moves = legalmoves(my_colour,board);
  tempboard = (int *)malloc(BOARDSIZE * sizeof(int));

  if (moves[0]== 0) {

    temp_move = -1;
    free(moves);

  }else if( moves[0]== 1) {
    temp_move = moves[1];
  } else {

    init_send = 1;

    for(j = 1; j <= moves[0] && init_send < size; j++){
      if((moves[j]== 11) || (moves[j]== 18) || ( moves[j] == 88) || (moves[j]== 81)){
        temp_move = moves[j];
      } else {

        loc_data.move = moves[j];
        MPI_Send(&loc_data, 1 , MPI_2INT, init_send , more_work , MPI_COMM_WORLD );
        i++;
        init_send++;

        if(init_send == size) {
          break;
          }

          if (i == moves[0]){
          break;
        }
      }
    }


    while(i <= moves[0]) {

        if((moves[i]== 11) || (moves[i]== 18) || ( moves[i] == 88) || (moves[i]== 81)){
          temp_move = moves[i];

        } else {

        loc_data.move = moves[i];
        memcpy(tempboard, board, BOARDSIZE * sizeof(int));
        makemove(loc_data.move, my_colour, tempboard);
        loc_data.score_diff = mini_max_alpha(tempboard , alpha , beta , MAX_DEPTH, my_colour);

        i++;
        recv++;

        if (loc_data.score_diff > maximum){
          maximum = loc_data.score_diff;
          temp_move = loc_data.move;

        }
      }
        MPI_Iprobe(MPI_ANY_SOURCE, done, MPI_COMM_WORLD, &msg_avail , &status);
        while(msg_avail){

            MPI_Recv(&loc_data, 1 , MPI_2INT, status.MPI_SOURCE,  done , MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            recv++;

            if (loc_data.score_diff > maximum){
              maximum = loc_data.score_diff;
              temp_move = loc_data.move;

            }

            if(i <= moves[0]){

              loc_data.move = moves[i];
              MPI_Send(&loc_data, 1 , MPI_2INT, status.MPI_SOURCE , more_work , MPI_COMM_WORLD );

              i++;
            }
            MPI_Iprobe(MPI_ANY_SOURCE, done, MPI_COMM_WORLD, &msg_avail , &status);
        }

      }
      free(tempboard);

      while (recv < moves[0]){

        MPI_Recv(&loc_data, 1 , MPI_2INT, MPI_ANY_SOURCE ,  done , MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        recv++;

        if (loc_data.score_diff > maximum){
          maximum = loc_data.score_diff;
          temp_move = loc_data.move;

        }

      }

      loc_data.score_diff = maximum;
      loc_data.move = temp_move;

      if(moves[0] > 1 ){

        for(int y = 1 ; y < size; y++){
          MPI_Send(&terminated, 1 , MPI_INT, y, 5000, MPI_COMM_WORLD );
        }

      }

    }

    if (temp_move == -1){
        move = "pass\n";

    } else {
        move = get_move_string(temp_move);
        makemove(temp_move, my_colour, board);
        free(moves);
      }

      return move;
      free(moves);

    }


/*
	Called when the other engine has made a move. The move is given in a
	string parameter of the form "xy", where x and y represent the row
	and column where the opponent's piece is placed, respectively.
 */
void play_move(char *move){
    int loc;
    if (my_colour == EMPTY){
        my_colour = WHITE;
    }
    if (strcmp(move, "pass") == 0){
        return;
    }
    loc = get_loc(move);
    makemove(loc, opponent(my_colour), board);
}

/*
	Called when the match is over.
 */
void game_over(){
    MPI_Finalize();
}

void send_board(){

}
char* get_move_string(int loc){
    static char ms[3];
    int row, col, new_loc;
    new_loc = loc - (9 + 2 * (loc / 10));
    row = new_loc / 8;
    col = new_loc % 8;
    ms[0] = row + '0';
    ms[1] = col + '0';
    ms[2] = '\n';
    return ms;
}

int get_loc(char* movestring){
    int row, col;
    row = movestring[0] - '0';
    col = movestring[1] - '0';
    return (10 * (row + 1)) + col + 1;
}

int count_legalmoves(int player, int *localboard){

  int *moves;
  int count;

  moves = legalmoves(player, localboard);
  count = 0;

  for(int i = 1 ; i <= moves[0]; i++){
    count++;
  }
  return count;
  free(moves);
}

int *legalmoves (int player, int *localboard) {
    int move, i, *moves;
    moves = (int *)malloc(65 * sizeof(int));
    moves[0] = 0;
    i = 0;
    for (move=11; move<=88; move++)
        if (legalp(move, player, localboard)) {
            i++;
            moves[i]=move;
        }
    moves[0]=i;
    return moves;
    free(moves);
}

int legalp (int move, int player, int *board) {
    int i;
    if (!validp(move)) return 0;
    if (board[move]==EMPTY) {
        i=0;
        while (i<=7 && !wouldflip(move, ALLDIRECTIONS[i], player, board)) i++;
        if (i==8) return 0; else return 1;
    }
    else return 0;
}

int validp (int move) {
    if ((move >= 11) && (move <= 88) && (move%10 >= 1) && (move%10 <= 8))
        return 1;
    else return 0;
}

int wouldflip (int move, int dir, int player, int *board) {
    int c;
    c = move + dir;
    if (board[c] == opponent(player))
        return findbracketingpiece(c+dir, dir, player, board);
    else return 0;
}

int findbracketingpiece(int square, int dir, int player, int *board) {
    while (board[square] == opponent(player)) square = square + dir;
    if (board[square] == player) return square;
    else return 0;
}

int opponent (int player) {
    switch (player) {

        case 1: return 2;
        case 2: return 1;

        default: printf("illegal player\n"); return 0;
    }

}

void makemove (int move, int player, int *board) {
    int i;
    board[move] = player;
    for (i=0; i<=7; i++) makeflips(move, ALLDIRECTIONS[i], player, board);
}

void makeflips (int move, int dir, int player, int *board) {
    int bracketer, c;
    bracketer = wouldflip(move, dir, player, board);
    if (bracketer) {
        c = move + dir;
        do {
            board[c] = player;
            c = c + dir;
        } while (c != bracketer);
    }
}

void printboard(){
    int row, col;
    fprintf(fp,"   1 2 3 4 5 6 7 8 [%c=%d %c=%d]\n",
            nameof(BLACK), count(BLACK, board), nameof(WHITE), count(WHITE, board));
    for (row=1; row<=8; row++) {
        fprintf(fp,"%d  ", row);
        for (col=1; col<=8; col++)
            fprintf(fp,"%c ", nameof(board[col + (10 * row)]));
        fprintf(fp,"\n");
    }
    fflush(fp);
}
void printlocalboard(int *localboard){
  int row, col;
  fprintf(fp,"   1 2 3 4 5 6 7 8 [%c=%d %c=%d]\n",
          nameof(BLACK), count(BLACK, localboard), nameof(WHITE), count(WHITE, localboard));
  for (row=1; row<=8; row++) {
      fprintf(fp,"%d  ", row);
      for (col=1; col<=8; col++)
          fprintf(fp,"%c ", nameof(localboard[col + (10 * row)]));
      fprintf(fp,"\n");
  }
  fflush(fp);
}



char nameof (int piece) {
    static char piecenames[5] = ".bw?";
    return(piecenames[piece]);
}

int count (int player, int * board) {

    int i, cnt;
    cnt=0;
    for (i=1; i<=88; i++)
        if (board[i] == player) cnt++;
    return cnt;
}
